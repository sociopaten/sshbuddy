release:
	cargo build --release
	cp -f target/release/sshbuddy /usr/local/bin/
	chmod +x /usr/local/bin/sshbuddy
	touch ~/.sshbuddy.sh
