use std::str::FromStr;

#[derive(Debug)]
pub enum Command {
    Add,
    Delete,
    List,
}

impl FromStr for Command {
    type Err = ();
    fn from_str(s: &str) -> Result<Command, ()> {
        match s {
            "add" => Ok(Command::Add),
            "delete" => Ok(Command::Delete),
            "list" => Ok(Command::List),
            _ => Err(()),
        }
    }
}
