mod alias;
mod command;
mod ssh;

use crate::command::Command;
use crate::ssh::{add, delete, list};
use clap::{App, Arg};
use std::process;

// TODO: Add new ssh alias
// TODO: Remove an alias

static VALID_COMMANDS: &'static str = "One of: add, delete, list, connect";

fn main() {
    let matches = App::new("sshbuddy")
        .version("0.1.0")
        .author("David jobe <daavid.jobe@gmail.com>")
        .arg(
            Arg::with_name("command")
                .required(true)
                .takes_value(true)
                .index(1)
                .help(VALID_COMMANDS),
        )
        .get_matches();

    match matches.value_of("command").map(|s| s.parse::<Command>()) {
        Some(Ok(command)) => match command {
            Command::List => {
                if let Ok(()) = list() {
                    process::exit(0);
                } else {
                    process::exit(1);
                }
            }
            Command::Add => {
                if let Ok(()) = add() {
                    process::exit(0);
                } else {
                    process::exit(1);
                }
            }
            Command::Delete => {
                println!("{}", delete());
            }
        },
        _ => println!("Command required, valid commands: {}", VALID_COMMANDS),
    }
}
