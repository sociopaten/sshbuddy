use colored::*;
use std::fmt;

pub struct Alias {
    name: String,
    user: String,
    ip: String,
}

impl Alias {
    pub fn new(name: String, user: String, ip: String) -> Alias {
        Alias {
            name: name.trim().to_string(),
            user: user.trim().to_string(),
            ip: ip.trim().to_string(),
        }
    }

    pub fn from_str(s: String) -> Result<Alias, ()> {
        let s = s.replace("alias ", "").replace("\"", "");

        let parts: Vec<&str> = s.split("=").collect();

        let name = parts.first().unwrap().to_string();

        let parts: Vec<&str> = parts.get(1).unwrap().split("@").collect();

        let user = parts.first().unwrap().replace("ssh ", "").to_string();

        let ip = parts.get(1).unwrap().to_string();

        Ok(Alias { name, user, ip })
    }

    pub fn to_bash_string(&self) -> String {
        format!("alias {}=\"ssh {}@{}\"\n", self.name, self.user, self.ip)
    }
}

impl fmt::Display for Alias {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_fmt(format_args!(
            "{}: {} | {}: {} | {}: {}",
            "Name",
            self.name.cyan(),
            "User",
            self.user.blue(),
            "IP",
            self.ip.yellow()
        ))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_to_bash_string() {
        match Alias::from_str("alias name = \"ssh sshuser@locahost\"".to_string()) {
            Ok(s) => assert!(true, s),
            Err(e) => assert!(false, e),
        }
    }
}
