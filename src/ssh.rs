use colored::*;
use dirs::home_dir;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::{stdin, BufRead, BufReader, Result};
use std::path::PathBuf;

use std::io::prelude::*;

use crate::alias::Alias;

pub fn list() -> Result<()> {
    let f = File::open(get_store_path())?;
    let file = BufReader::new(&f);

    println!(
        "\n{}",
        "-- Connections ----------------------------------------"
            .cyan()
            .bold()
    );
    for line in file.lines() {
        let l = line.unwrap();
        if let Ok(alias) = Alias::from_str(l) {
            println!("{} {}", "-".red(), alias);
        }
    }
    println!(
        "{}",
        "-------------------------------------------------------"
            .cyan()
            .bold()
    );
    Ok(())
}

pub fn add() -> Result<()> {
    let mut name = String::new();
    println!("{}", "Name of connection:".green().bold());
    stdin()
        .read_line(&mut name)
        .ok()
        .expect("Failed to read name");

    let mut user = String::new();
    println!("{}", "SSH user:".green().bold());
    stdin()
        .read_line(&mut user)
        .ok()
        .expect("Failed to read user");

    let mut ip = String::new();
    println!("{}", "IP or domain:".green().bold());
    stdin().read_line(&mut ip).ok().expect("Failed to read ip");

    let alias = Alias::new(name, user, ip);

    let mut file = OpenOptions::new()
        .write(true)
        .append(true)
        .open(get_store_path())
        .unwrap();

    write!(file, "{}", alias.to_bash_string())?;

    Ok(())
}

pub fn delete() -> String {
    String::from("Not implemented")
}

fn get_store_path() -> PathBuf {
    let mut path = PathBuf::new();
    if let Some(home) = home_dir() {
        path.push(home);
    }
    path.push(".sshbuddy.sh");
    path
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_list() {
        match list() {
            Ok(()) => assert!(true, "File exists."),
            Err(e) => assert!(false, "Error: {}", e),
        };
    }
}
